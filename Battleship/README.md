﻿# Battleship Exercise

The main class to instantiate is **Gameboard**. Set up the gameboard by addings ships with the **AddShip()** method. Then you can attempt to shoot them with the **TakeAttack()** method.

Refer to the test project **Battleship.Tests** to understand the usage of the Gameboard and all of the interacting classes.

&copy; Leigh Alsteris, August 2018
