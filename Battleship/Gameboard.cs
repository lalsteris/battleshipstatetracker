﻿using System.Collections.Generic;
using System.Linq;

namespace Battleship
{
	/// <summary>
	/// Represents that Battleship gameboard (and it's state) for a single player.
	/// </summary>
	public class Gameboard
	{
		/// <summary>
		/// Create a gameboard.
		/// </summary>
		/// <param name="xwidth"></param>
		/// <param name="ywidth"></param>
		public Gameboard(int xwidth = 10, int ywidth = 10)
		{
			Xwidth = xwidth;
			Ywidth = ywidth;
			PositionsLookup = InitialiseSquareMatrix(xwidth, ywidth);
			Positions = PositionsLookup.Values.SelectMany(x => x.Values).OrderBy(p => p.Y).ThenBy(p => p.X).ToList();
		}

		/// <summary>
		/// Returns whether the player has lost the game yet (i.e., all battleships are sunk).
		/// </summary>
		public bool IsGameOver => Ships.TrueForAll(x => x.IsSunk);

		/// <summary>
		/// Convenient access to all ships on the Gameboard. Useful for determining the status of all ships.
		/// </summary>
		public List<Ship> Ships { get; } = new List<Ship>();

		/// <summary>
		/// Convenient access to all positions. Useful for rendering to a user interface.
		/// </summary>
		public List<Position> Positions { get; set; }

		/// <summary>
		/// Indexed positions for efficient lookup.
		/// </summary>
		public IDictionary<int, IDictionary<int, Position>> PositionsLookup { get; }

		public int Xwidth { get; }

		public int Ywidth { get; }

		/// <summary>
		/// Add a ship to the board.
		/// </summary>
		/// <param name="length"></param>
		/// <param name="orientation"></param>
		/// <param name="x">X coordinate</param>
		/// <param name="y">Y coordinate</param>
		/// <returns></returns>
		public AddShipResult AddShip(int length, Orientation orientation, int x, int y)
		{
			if (x <= 0 || x > Xwidth) return AddShipResult.OutOfBounds();
			if (y <= 0 || y > Ywidth) return AddShipResult.OutOfBounds();

			List<Position> shipSquares;

			// ReSharper disable once ConvertIfStatementToSwitchStatement
			if (orientation == Orientation.Horizontal)
			{
				if (Xwidth < x + length - 1) return AddShipResult.OutOfBounds();
				shipSquares = Enumerable.Range(x, length).Select(idx => GetPosition(idx, y)).ToList();
			}
			else
			{
				if (Ywidth < y + length - 1) return AddShipResult.OutOfBounds();
				shipSquares = Enumerable.Range(y, length).Select(idx => GetPosition(x, idx)).ToList();
			}

			var occupiedSquares = shipSquares.Where(square => square.IsOccupied).ToArray();

			if (occupiedSquares.Any()) return AddShipResult.Occupied(occupiedSquares);

			var ship = new Ship(Ships.Count - 1, shipSquares);
			Ships.Add(ship);
			shipSquares.ForEach(square => square.PlaceShip(ship));
			return AddShipResult.Added(ship);
		}

		/// <summary>
		/// Take an "attack" at a given position.
		/// </summary>
		/// <param name="x">X coordinate</param>
		/// <param name="y">Y coordinate</param>
		/// <returns>The outcome of the attack.</returns>
		public AttackResult TakeAttack(int x, int y)
		{
			if (IsGameOver) return AttackResult.GameOver;
			var position = GetPosition(x, y);
			if (position == null || position.IsAttacked) return AttackResult.Invalid;
			position.Attack();
			if (!position.IsOccupied) return AttackResult.Miss;
			if (IsGameOver) return AttackResult.GameOver;
			return position.Ship.IsSunk ? AttackResult.Sunk : AttackResult.Hit;
		}

		private Position GetPosition(int x, int y)
		{
			if (x <= 0 || x > Xwidth) return null;
			if (y <= 0 || y > Ywidth) return null;
			return PositionsLookup[x][y];
		}

		private static IDictionary<int, IDictionary<int, Position>> InitialiseSquareMatrix(int xwidth, int ywidth)
		{
			var squares = new Dictionary<int, IDictionary<int, Position>>();

			for (var i = 1; i <= xwidth; i++)
			{
				squares.Add(i, new Dictionary<int, Position>());
				for (var j = 1; j <= ywidth; j++) squares[i].Add(j, new Position(i, j));
			}

			return squares;
		}
	}
}