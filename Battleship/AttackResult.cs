﻿namespace Battleship
{
	public enum AttackResult
	{
		Invalid = 0,
		Miss = 1,
		Hit = 2,
		Sunk = 3,
		GameOver = 4
	}
}