﻿using System.Collections.Generic;
using System.Linq;

namespace Battleship
{
	/// <summary>
	/// Represents a ship on the Battelship Gameboard.
	/// </summary>
	public class Ship
	{
		internal Ship(int id, IReadOnlyList<Position> positions)
		{
			Id = id;
			Positions = positions;
		}

		public int Id { get; }
		public bool IsSunk => Positions.All(s => s.IsAttacked);
		public int Length => Positions.Count;
		public int NumberOfHits => Positions.Count(p => p.IsAttacked);

		/// <summary>
		/// The Battleship Gameboard positions which this ship occupies.
		/// </summary>
		public IReadOnlyList<Position> Positions { get; }
	}
}