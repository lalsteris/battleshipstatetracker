﻿using System.Collections.Generic;
using System.Collections.Immutable;

namespace Battleship
{
	public class AddShipResult
	{
		private AddShipResult()
		{
		}

		public bool IsSuccessful => Ship != null;

		public bool IsOutOfBounds => !IsSuccessful && OccuppiedPositions == null;

		public ImmutableList<Position> OccuppiedPositions { get; private set; }

		public Ship Ship { get; private set; }

		internal static AddShipResult Added(Ship ship)
		{
			return new AddShipResult {Ship = ship};
		}

		internal static AddShipResult Occupied(IEnumerable<Position> occuppiedPositions)
		{
			return new AddShipResult {OccuppiedPositions = occuppiedPositions.ToImmutableList()};
		}

		internal static AddShipResult OutOfBounds()
		{
			return new AddShipResult();
		}
	}
}