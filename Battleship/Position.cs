﻿using System;

namespace Battleship
{
	/// <summary>
	/// Represents a position (i.e., a unique set of coordinates) on the Battleship gameboard.
	/// </summary>
	public class Position
	{
		internal Position(int x, int y)
		{
			X = x;
			Y = y;
		}

		public bool IsAttacked { get; private set; }
		public bool IsOccupied => Ship != null;
		public Ship Ship { get; private set; }
		public int X { get; }
		public int Y { get; }

		internal void Attack()
		{
			if (IsAttacked) throw new InvalidOperationException();
			IsAttacked = true;
		}

		internal void PlaceShip(Ship ship)
		{
			if (IsOccupied) throw new InvalidOperationException();
			Ship = ship;
		}
	}
}