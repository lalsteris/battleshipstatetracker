using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Battleship.Tests
{
	[TestClass]
	public class GameBoard_Should
	{
		[DataTestMethod]
		[DataRow(10, 10)]
		[DataRow(20, 15)]
		public void Initialise_With_An_Area_Of_N_By_M(int n, int m)
		{
			var gameboard = new Gameboard(n, m);

			Assert.AreEqual(n, gameboard.Xwidth);
			Assert.AreEqual(m, gameboard.Ywidth);
			Assert.AreEqual(gameboard.Positions.Min(p => p.X), 1);
			Assert.AreEqual(gameboard.Positions.Min(p => p.Y), 1);
			Assert.AreEqual(gameboard.Positions.Max(p => p.X), n);
			Assert.AreEqual(gameboard.Positions.Max(p => p.Y), m);
		}

		[DataTestMethod]
		[DataRow(5, Orientation.Horizontal, 2, 2)]
		[DataRow(2, Orientation.Horizontal, 5, 2)]
		[DataRow(4, Orientation.Vertical, 3, 7)]
		[DataRow(3, Orientation.Vertical, 8, 4)]
		[DataRow(1, Orientation.Vertical, 10, 10)]
		[DataRow(2, Orientation.Vertical, 10, 3)]
		[DataRow(5, Orientation.Horizontal, 2, 10)]
		public void Add_A_Ship_Successfully_When_Coordinates_Are_Fully_Within_GameBoard_Area(int length,
			Orientation orientation, int x, int y)
		{
			var gameboard = new Gameboard(10, 10);

			var addShipResult = gameboard.AddShip(length, orientation, x, y);

			Assert.IsTrue(addShipResult.IsSuccessful);
			Assert.AreEqual(length, addShipResult.Ship.Length);
			Assert.IsTrue(addShipResult.Ship.Positions.First().X <= gameboard.Positions.Max(p => p.X));
			Assert.IsTrue(addShipResult.Ship.Positions.Last().X <= gameboard.Positions.Max(p => p.X));
			Assert.IsTrue(addShipResult.Ship.Positions.First().Y <= gameboard.Positions.Max(p => p.Y));
			Assert.IsTrue(addShipResult.Ship.Positions.Last().Y <= gameboard.Positions.Max(p => p.Y));
		}

		[DataTestMethod]
		[DataRow(5, Orientation.Horizontal, 7, 2)]
		[DataRow(2, Orientation.Horizontal, 10, 2)]
		[DataRow(4, Orientation.Vertical, 3, 8)]
		[DataRow(3, Orientation.Vertical, 8, 9)]
		[DataRow(3, Orientation.Vertical, 0, 1)]
		[DataRow(3, Orientation.Vertical, 1, 0)]
		public void Not_Add_A_Ship_When_Coordinates_Are_Outside_GameBoard_Area(int length, Orientation orientation, int x,
			int y)
		{
			var gameboard = new Gameboard(10, 10);

			var addShipResult = gameboard.AddShip(length, orientation, x, y);

			Assert.IsFalse(addShipResult.IsSuccessful);
			Assert.IsTrue(addShipResult.IsOutOfBounds);
		}

		[DataTestMethod]
		[DataRow(5, Orientation.Vertical, 4, 2, 4, Orientation.Horizontal, 3, 3)]
		[DataRow(3, Orientation.Horizontal, 5, 5, 3, Orientation.Vertical, 7, 4)]
		public void Not_Add_A_Ship_When_It_Overlaps_Another(
			int length1, Orientation orientation1, int x1, int y1,
			int length2, Orientation orientation2, int x2, int y2)
		{
			var gameboard = new Gameboard(10, 10);

			var addFirstShipResult = gameboard.AddShip(length1, orientation1, x1, y1);
			var addSecondShipResult = gameboard.AddShip(length2, orientation2, x2, y2);

			Assert.IsTrue(addFirstShipResult.IsSuccessful);
			Assert.IsFalse(addSecondShipResult.IsSuccessful);
			Assert.IsTrue(addSecondShipResult.OccuppiedPositions.All(p => p.Ship == addFirstShipResult.Ship));
		}

		[TestMethod]
		public void TakeAttack()
		{
			var gameboard = new Gameboard();
			var ship1 = gameboard.AddShip(3, Orientation.Horizontal, 6, 2).Ship;
			var ship2 = gameboard.AddShip(2, Orientation.Vertical, 3, 6).Ship;

			Assert.AreEqual(0, ship1.NumberOfHits);
			Assert.AreEqual(0, ship2.NumberOfHits);

			Assert.IsTrue(gameboard.TakeAttack(6, 2) == AttackResult.Hit);
			Assert.AreEqual(1, ship1.NumberOfHits);

			Assert.IsTrue(gameboard.TakeAttack(7, 2) == AttackResult.Hit);
			Assert.AreEqual(2, ship1.NumberOfHits);

			Assert.IsTrue(gameboard.TakeAttack(8, 2) == AttackResult.Sunk);
			Assert.AreEqual(3, ship1.NumberOfHits);

			Assert.IsTrue(gameboard.TakeAttack(9, 2) == AttackResult.Miss);

			Assert.IsTrue(gameboard.TakeAttack(12, 12) == AttackResult.Invalid);

			Assert.IsTrue(gameboard.TakeAttack(3, 6) == AttackResult.Hit);
			Assert.AreEqual(1, ship2.NumberOfHits);

			Assert.IsTrue(gameboard.TakeAttack(3, 7) == AttackResult.GameOver);
			Assert.AreEqual(2, ship2.NumberOfHits);

			Assert.IsTrue(gameboard.IsGameOver);
		}
	}
}